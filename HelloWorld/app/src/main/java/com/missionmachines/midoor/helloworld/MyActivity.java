package com.missionmachines.midoor.helloworld;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import java.net.HttpURLConnection;
import javax.net.ssl.HttpsURLConnection;


public class MyActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.midoor.missionmachines.com.first_version.MESSAGE";
    public static final String TAG = "Network Connect";
    private TextView textView;
    private CheckBox https;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        switch (id) {
            case R.id.action_clean:
                String empty = "";
                textView.setText(empty);
                return true;

            }

        return false;
        /**
         * original code
         * return super.onOptionsItemSelected(item);
         */

    }

    public void saveData(View view) {

        // get host from text edit
        EditText server = (EditText) findViewById(R.id.edit_server);
        String message = server.getText().toString();

        // get port from text edit
        EditText port = (EditText) findViewById(R.id.edit_port);
        message += "\n";
        message += port.getText().toString();

        // get serialNumber from text edit
        EditText serialNumber = (EditText) findViewById(R.id.edit_serialNumber);
        message += "\n";
        message += serialNumber.getText().toString();

        // get key from text edit
        EditText key = (EditText) findViewById(R.id.edit_key);
        message += "\n";
        message += key.getText().toString();

        /**
         * choice the protocol of url connection https / http
         */
        https = (CheckBox) findViewById(R.id.https);
        String prefix = null;
        if(https.isChecked()) {
            prefix = "https";
        } else {
            prefix = "http";
        }

        Intent intent = new Intent(this, DisplayMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE,
                                        prefix +
                                        "\n------------------\n" +
                                                message);
        startActivity(intent);
    }


    public void sendMessage(View view) throws IOException {
        textView = (TextView) findViewById(R.id.text);

        // get host from text edit
        EditText server = (EditText) findViewById(R.id.edit_server);
        String serverMsg = server.getText().toString();

        // get port from text edit
        EditText port = (EditText) findViewById(R.id.edit_port);
        String portMsg = port.getText().toString();

        // get serialNumber from text edit
        EditText serialNumber = (EditText) findViewById(R.id.edit_serialNumber);
        String serialNumberMsg = serialNumber.getText().toString();

        // get key from text edit
        EditText key = (EditText) findViewById(R.id.edit_key);
        String keyMsg = key.getText().toString();

        /**
         * choice the protocol of url connection https / http
         */
        https = (CheckBox) findViewById(R.id.https);

        String prefix = null;
        if(https.isChecked()) {
            prefix = "https://";
        } else {
            prefix = "http://";
        }

        String suffix;
        if(portMsg.toString() == "80") {
            suffix = "/";
        } else {
            suffix = ":"+portMsg;
        }

        /**
         * Open Connection with url
         */
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            new CallServer().execute(prefix +serverMsg+ suffix);
        } else {
        }
        textView.setText(textView.getText() + "\n" + prefix +serverMsg + suffix);
    }

    private class CallServer extends AsyncTask<String, Void, String> {

        private Exception exception;
        protected String doInBackground (String... urls) {
            try {
                return downloadUrl(urls[0]);
            } catch (Exception e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        protected void onPostExecute(String result) {
            textView.setText(textView.getText() + "\n" + result);
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;

        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 1500;
        try {
            URL url = new URL(myurl);

            /**
             * fix redundance problem in the future
             */
            if(https.isChecked()) {
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("DEBUG_TAG: ", "The response is: " + response);
                is = conn.getInputStream();
            } else {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("DEBUG_TAG: ", "The response is: " + response);
                is = conn.getInputStream();
            }

            // Convert the InputStream into a string
            String contentAsString = readIt(is, len);
            return contentAsString;
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }
}

